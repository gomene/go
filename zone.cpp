#include "listeaction.cpp"

class zone{

	//Attributs:
	public: listePion* membres;
	public: listePion* libertes;   

	//Constructeur de la classe
	public: zone(){
    	this->membres = NULL;
    	this->libertes = NULL;
	}

  	public: zone(int i, int j){
		this->membres = new listePion(i,j);
		this->libertes = NULL ;
	}

	//Destructeur de la liste
	public: ~zone(){
		delete(membres);
		delete(libertes);
	}

	//----------Méthodes---------
	//Ajoute une nouvelle liberte
 	bool NouvelleLiberte(int i,int j){
		if(libertes != NULL){
			libertes->NouveauPion(i,j);
			return true;
		}else{
			libertes = new listePion(i,j);
		}
	}

	//Supprime une liberté et appelle supprimerZone s'il n'y a plus de liberté
	bool SupprimerLiberte(int i, int j){
		if(libertes != NULL){
			if((libertes->i == i) && (libertes->j == j)){
				if(libertes->suivant == NULL){
					//std::cout << i << "," << j << libertes;
					this->libertes = NULL;
				}else{
					listePion* oldlib = libertes;
					libertes = libertes->suivant;
					oldlib->suivant = NULL;
					delete(oldlib);
				}
			}else{
				libertes->SupprimerPion(i,j);
			}
		}else{
			return false;
		}
		return true;
	}
	
};
