#include <iostream>

class listePion{
	public: int i;
	public: int j;
	public: listePion* suivant;
  //la valeur i des suivant est toujours >= à celui de l'objet instancié.

	public: listePion(int moni,int monj){
		this->i = moni;
		this->j = monj;
	}

	public: listePion(){}

	public: ~listePion(){
		if (suivant != NULL){
    		delete(suivant);
    	}
  	}

	int Longeur(){
		int comp =0;
		listePion* listSuivant= this->suivant;
		while(listSuivant != NULL){
			comp = comp+1;
			listSuivant = listSuivant->suivant;
		}
		return (comp+1);
	}
  
    bool FusionListe(listePion* l){
	 	//std::cout << "\n liste à fusionné \n" << this << "et :\n" << l;
		if(l == NULL){
			//std::cout << "donne \n" << this;
			return false;
	  	}else {
			int is,js;
			for(listePion* list = l; list != NULL; list = list->suivant){
				is = list->i;
				js = list->j;
				this->NouveauPion(is,js);
			}
			//std::cout << "donne \n" << this;
			return true;
		}
	}
  
  
  
	bool NouveauPion(int moni, int monj){
		if(i == moni && j == monj){
			return true;
		}else if((i == moni & j > monj) || i > moni){
    		listePion* Nouveausuivant = new listePion(i,j);
			Nouveausuivant->suivant = this->suivant;
			this->suivant = Nouveausuivant;
			this->i = moni;
			this->j = monj;
			return true;
    	}else if (suivant != NULL){
    		return suivant->NouveauPion(moni,monj);
    	}else if (suivant == NULL){
    	 	suivant = new listePion(moni,monj);
			return true;
    	}
  	}

	bool SupprimerPion(int moni, int monj){
    	if(i == moni && j == monj){
			if(this->suivant != NULL){
				listePion* listesupprime = this->suivant;
				this->suivant = listesupprime->suivant;
				this->i = listesupprime->i;
				this->j = listesupprime->j;
				listesupprime->suivant = NULL;
				delete(listesupprime);
				return true;
			}else{
				delete(this);
				return false;
			}
    	}else if((i == moni && j > monj) || i > moni){
			return true;
    	}else if (suivant != NULL){
			bool retour = suivant->SupprimerPion(moni,monj);
			if(!retour){
				this->suivant = NULL;
				return true;
			};
		}
		return true;
    	
	}

	friend std::ostream& operator<<(std::ostream& os, listePion* listepion);

};
  
std::ostream& operator<<(std::ostream& os, listePion* listepion){
	os << "  [ ";
	for(listePion* list = listepion; list != NULL; list  = list ->suivant){
		os << "("<<list->i << "," << list->j << ") ";
	}
	os <<"]\n";
	return os;
}
