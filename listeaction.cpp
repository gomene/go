#include <iostream>
#include "listepion.cpp" 

class listeAction{

	//Attributs
	public: bool passer;
	public: int nTour;
	public: listeAction* toursuiv;
	public: listeAction* tourprec;
	public: int i; public: int j; //position du pion joué.
	public: listePion* toDel; //Contient les pions supprimé pendant le tour. 

	listeAction(int nT, bool passer=false){
		this->nTour = nT;
	}

	listeAction(listeAction* trprec,bool passer=false){
		tourprec = trprec;
		nTour = trprec->nTour+1;
		delete(trprec->toursuiv);
		trprec->toursuiv = this;
	}

	~listeAction(){
		if(toursuiv != NULL)
			delete(toursuiv);
		if(toDel != NULL)
			delete(toDel);
	}
};
