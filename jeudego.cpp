#include <iostream>
#include <string>
#include "pion.cpp"

using namespace std;

class jeudego{

	public: static const int intmax = 9;
	public: pion* Tab[9][9];
	public: int tour;
	public: int compt;
	public: listeAction* premierTour;
	public: listeAction* dernierTour;
	public: int blanc = 0;
	public: int noir = 0;
	//public: listeAction* NosActions;


	//-------------------------------------
	//Méthodes
	//-------------------------------------

	//------Constructeur-------------------
	public: jeudego(){
		for (int i=0;i<intmax;i++){
			for (int j=0;j<intmax;j++){
			Tab[i][j]=NULL;
			};
		};

		premierTour = new listeAction(0);
		dernierTour = new listeAction(premierTour);
		dernierTour = premierTour;
		tour = premierTour->nTour;
	};

	//------Déstructeur--------------------
	public: ~jeudego(){
		for (int i=0;i<intmax;i++){
		for (int j=0;j<intmax;j++){
			delete(Tab[i][j]);
		}
		}
	}

	//------Méthodes------------------------

	bool fusion(zone* mazone, zone* sazone, int id, int jd){
		sazone->libertes->FusionListe(mazone->libertes);
		sazone->membres->FusionListe(mazone->membres);
		sazone->SupprimerLiberte(id,jd);
		
		int is,js;

		for(listePion* list = mazone->membres; list !=  NULL; list = list->suivant){
			  is= list->i;
			  js= list->j;
			  Tab[is][js]->mazone = sazone;
		}

		return true;
	}

	bool Jouer(int i, int j){
		bool color = (tour & 1); // test la parité du tour.	
		dernierTour->i = i;
		dernierTour->j = j;
		//if(i==0 && j==0) return false;
		if(Tab[i][j] != NULL) return false;
		Ajouter(i,j,color);
		/*Si l'endroit ou le pion à été joué n'est pas valide */
		//cout << Tab[i][j]->mazone->libertes;
		
		if(Tab[i][j]->mazone->libertes == NULL){
			annuler();
			cout << "Pas d'action suicidaire ! \n";
			dernierTour->toDel = NULL;
			return false;
		}
		if(tour>0 && !dernierTour->tourprec->passer){
			listePion* toDel = dernierTour->toDel;
			listePion* toDelold = dernierTour->tourprec->toDel;
			if(toDel != NULL && toDelold != NULL){
			if(toDel->suivant == NULL && toDelold->suivant == NULL){
			if(toDelold->i == dernierTour->i && toDelold->j == dernierTour->j){
			if(toDel->i == dernierTour->tourprec->i && toDel->j == dernierTour->tourprec->j){
				Tab[i][j] = NULL;
				dernierTour->toDel = NULL;
				Ajouter(toDel->i,toDel->j,!color);
				dernierTour->toDel = NULL;
				cout << "Ce coup n'est pas bon, la régle du KO interdit de ramener à la situation précédante. \n";
				return false;
			}
			}
			}
			}
		}

		if(dernierTour->toDel != NULL){
			if(tour & 1){
				blanc += dernierTour->toDel->Longeur();
			}else{
				noir += dernierTour->toDel->Longeur();
			}
		}
		tour++;
		dernierTour = new listeAction(dernierTour);	
	}

	bool annuler(){
		int i = dernierTour->i;
		int j = dernierTour->j;
		bool color = (tour & 1);

		for(listePion* toDel = dernierTour->toDel; toDel != NULL; toDel->suivant){
	                Ajouter(i,j,!color);
                }
        Tab[i][j]->mazone->NouvelleLiberte(i,j);
        Tab[i][j]->mazone->membres->SupprimerPion(i,j);
        delete(Tab[i][j]);
        Tab[i][j] = NULL;
        if(i>0 && Tab[i-1][j]->color != color){
			Tab[i-1][j]->mazone->NouvelleLiberte(i,j);}
		if(j>0 && Tab[i][j-1]->color != color){
			Tab[i][j-1]->mazone->NouvelleLiberte(i,j);}
		if(i<(intmax-1) && Tab[i+1][j]->color != color){
			Tab[i+1][j]->mazone->NouvelleLiberte(i,j);}
		if(j<(intmax-1) && Tab[i][j+1]->color != color){
			Tab[i][j+1]->mazone->NouvelleLiberte(i,j);}
        return true;
	}


	bool passer(){
		tour++;
		dernierTour->passer = true;
		dernierTour = new listeAction(dernierTour, true);
		if (dernierTour->tourprec->tourprec->passer) return true;
		return false;
	}

	bool Ajouter(int i, int j, bool couleur){
		//cout << "Le pion ajouter est en" << i << "," <<j<< " \n";
		//Sinon, contrôle l'état de toute les autres cases.
		Tab[i][j] = new pion(couleur,new zone(i,j));

		if(i>0)
			miseajour(i,j,i-1,j,couleur);
		if(j>0)
			miseajour(i,j,i,j-1,couleur);
		if(i<(intmax-1))
			miseajour(i,j,i+1,j,couleur);
		if(j<(intmax-1))
			miseajour(i,j,i,j+1,couleur);

		int is;
		int js;
		int oldcompt = compt;
		for(listePion* toDel = dernierTour->toDel; toDel != NULL; toDel = toDel->suivant){
			is = toDel->i;
			js = toDel->j;
			compt +=1;
			//cout << "supprime " << i << "," << j << "\n";
			if(Tab[is][js] != NULL){
				Tab[is][js] = NULL;}
		}
		if(compt - oldcompt != 0){
		cout << "Capture " << compt - oldcompt << " pions " << ((tour & 1)?"noirs":"blancs") <<"\n";}
	}

	bool miseajour(int i,int j,int k,int l, bool couleur){
		if (Tab[k][l] == NULL){
			//cout <<"\033[1;34mLiberte en  (" << k<<","<<l<<")\n\033[0m";
			Tab[i][j]->mazone->NouvelleLiberte(k,l);
		}else if (Tab[k][l]->color == couleur){
			if(Tab[i][j]->mazone == Tab[k][l]->mazone) return true;
			//cout << "\033[1;34mFusion avec (" << k<<","<<l<<")\n\033[0m";
			this->fusion(Tab[i][j]->mazone,Tab[k][l]->mazone,i,j);
		}else if (Tab[k][l]->color != couleur){
			if(Tab[k][l]->mazone->libertes == NULL) return false;
			//cout <<"\033[1;34mRm lib en   (" << k<<","<<l<<")\n\033[0m";
			Tab[k][l]->mazone->SupprimerLiberte(i,j);
			if(Tab[k][l]->mazone->libertes == NULL){
				remove(k,l,couleur);
			}
		}
	}

	bool remove(int k,int l, bool couleur){
		listePion* recept = dernierTour->toDel;
		listePion* envoi = Tab[k][l]->mazone->membres;
		if(recept == NULL){
			if(envoi != NULL){
				dernierTour->toDel = new listePion(envoi->i,envoi->j);
				envoi = envoi->suivant;
				recept = dernierTour->toDel;
			}
		}
		if(recept != NULL){
			recept->FusionListe(envoi);
		}
		removetool(Tab[k][l]->mazone->membres, !couleur);
		return true;
	}

	bool removetool(listePion* toDelete, bool couleur){
		int is;
		int js;
		for(listePion* liste = toDelete;liste != NULL;liste=liste->suivant){
			is = liste->i;
			js = liste->j;
			maj(is,js,couleur);
		}
		delete(toDelete);
		return true;
	}

	bool maj(int i, int j, 	bool couleur){
		if(i>0)
			majtool(i,j,i-1,j,couleur);
		if(j>0)
			majtool(i,j,i,j-1,couleur);
		if(i<(intmax-1))
			majtool(i,j,i+1,j,couleur);
		if(j<(intmax-1))
			majtool(i,j,i,j+1,couleur);
		return true;
	}

	bool majtool(int i,int j,int k,int l,bool couleur){
		if(Tab[k][l] != NULL){
			if(Tab[k][l]->color != couleur){
				Tab[k][l]->mazone->NouvelleLiberte(i,j);
			}
		}
		return true;
	}
	
	bool CompCouleur(){
		int noir = 0;
		int blanc = 0;
		for(int i=0; i<intmax; i++){
			for(int j=0; j<intmax; j++){
				
				if(Tab[i][j] != NULL){
						if(Tab[i][j]->color == false){
						noir += 1;
						}
						else if (Tab[i][j]->color == true){
						blanc +=1;
						}
					}
			}
		}
		
		 cout<< "Le nombre de pion noir: " << noir << "\n";
		 cout<< "Le nombre de pion blanc: " << blanc << "\n";
		return true;
	}

	bool Affichage(pion* pt[][9]){
		int max = intmax;
	        cout << "\033[0;47;30m┏━━";
	        for (int j=0;j<max;j++){
	                cout << "━━";
	        }
                cout << "┓\033[0m\n";
	        cout << "\033[0;47;30m┃  ";
	        for (int j=0;j<max;j++) 
			cout << j << " ";
	        cout << "┃\033[0m\n";
	        for (int i=0;i<max;i++){
	                cout << "\033[0;47;30m┃" << i;
	                for (int j=0;j<max;j++){
	                        if(pt[i][j]==NULL){
	                                cout << "─┼";
	                        }else{
	                                if(pt[i][j]->color == true){
	                                        cout << "─\033[1;37;47m●\033[0;47;30m";
	                                }else{
	                                        cout << "─\033[0;30;47m●\033[0;47;30m";
	                                }
	                        }
	                }
	                cout << "─┃\033[0m\n";
	        }
	        cout << "\033[0;47;30m┗━━";
	        for (int j=0;j<max;j++){
	                cout << "━━";
	        }
                cout << "┛\033[0m\n";
	        cout << "\n";
	        return true;
	}
	
	bool revoir(){
	
	jeudego* go= new jeudego();
	
	int is,ii;
	int js,jj;
	bool c;
	for(listeAction* l = premierTour;l!= dernierTour; l=l->toursuiv){
		if (l->passer == false){
			is= l->i;
			js = l-> j;
			c = (l->nTour & 1);
			go->Tab[is][js] = new pion(c, NULL);
			if(l->toDel != NULL){
					for(listePion* liste = l->toDel;liste != NULL;liste=liste->suivant ){
						ii=liste->i;
						jj=liste->j;
						go->Tab[ii][jj] = NULL;
					}
			}
			cout << "Tour de "<< ((l->nTour & 1)? "blanc: ":"noir: " )<< "(" <<is << ", "<< js << ") \n";
			cout << go;
		}
		else{
			cout << "Tour de "<< ((l->nTour & 1)? "blanc: ":"noir: " )<<"Passer \n";	
			cout<< go;
		}
		}return true;
	
	}

	friend ostream& operator<<(ostream& os, jeudego* go);
};

bool startgame(jeudego *test, int intfin);

bool Begin(){
	jeudego* test = NULL;
	string input;
	string option;
	int i = -2;

	while(true){
		cout << "\033[2J";
		cout << "Bienvenue sur GO-mene, une implémentation des régles stragebourgeoise du go par My Nguyen Thi Tra et Alexandre Gris ! \n";
		cout << "\"Jouer\" pour jouer une nouvelle partie.\n";
		cout << "\"Quitter\" pour quitter le jeux.\n";
		if(test != NULL) cout << "\"Revoir\" pour revoir la derniére partie.\n";
		cin >> input;

		if(input =="Quitter") break;
		if((input =="Revoir") && (test != NULL)) {test->revoir(); cin >> option;}
		if((input =="Jouer")){
			test = new jeudego();
			while(i<1){
				cout << "Veuillez indiquer le nombre de prise avant la fin du jeux, -1 pour aucune limite.\n";
				cin >> option;
				try{
					i = stoi(option);
				}catch(...){}
				if(i==-1){
					i = 142857; //période de 1/7
				}
			}
			startgame(test,i);
			i == -2;
		}
	}
}

bool startgame(jeudego* test, int intfin){
	string myinput;
   	int lgt;
	int k;
	int i;
	int j;
	int t;
	int max = test->intmax;
	bool mytest;
	string::size_type sz;
	bool fin = false;
	cout << "\033[2J";
	while(!fin){
		cout << "C'est au joueur " << ((test->tour & 1)?"blanc":"noir") << " de jouer.\n> \"q\" pour quitter.\n> \"3,4\" pour jouer un pion en ligne 3, colonne 4..\n> \"passer\" pour passer.\n> \"revoir\" pour revoir la partie.\n\n";
		cin >> myinput;
        	if (myinput == "q") break;
	        if (myinput == "passer"){
				if(test->passer()){
					fin = true;
					test->CompCouleur();
					if(test->noir != test->blanc){
					cout<< "Les "<< ((test->blanc > test->blanc)?" blancs":" noirs") << " sont gagné la partie. \n";}
					else {cout<< "Les deux sont égaux. \n";}
				}
		}else{
			if (myinput == "revoir"){
				test->revoir();
			}else{
				lgt = myinput.length();
	        		k=0;
				mytest = true;
	        		while((k<lgt) && mytest){
	       				if(myinput[k]==','){
						mytest = false;
					}else{
						k++;
					}
		       	 	}
				if (k==lgt){
		            		cout << "not a regular expression \n";
				}else{
					try{
		        		        i = stoi(myinput.substr(0,k),&sz);
		                		j = stoi(myinput.substr(k+1,lgt-k-1),&sz);
			            		if(i<max && j<max){
							cout << "\033[2J";
							if(test->Jouer(i,j)){
								cout << "Le dernier mouvement était en (" << i << "," << j << ")\n \n";
								if(test->noir >= intfin || test->blanc >=intfin){
								fin = true;
								cout << "La partie est terminé, les " << (test->noir >= intfin?"noir":"blanc") << " ont gagné la partie avec un score de : ";
								if(test->noir >=intfin){
									cout << test->noir<<";"<<test->blanc;
								}else{
									cout << test->blanc<<";"<<test->noir;
								}
								}
							cout << test;
							}
		            			}else{
							cout << "Ce n'est pas sur le plateau \n";
						}
					}catch(...){
		                		cout << "Entrée non valide ! \n";
		           		}
		        	}
			}
		}
	}

	cout << "La partie est terminé !\n";
	cin >> myinput;

}

ostream& operator<<(ostream& os, jeudego* go){
	int max = go->intmax;
	
	os << "\n\033[0;47;30m┏━━";
	for (int j=0;j<max;j++){
		os << "━━";
	}
		os << "┓\033[0m\n";
	os << "\033[0;47;30m┃  ";
	for (int j=0;j<max;j++) os << j << " ";
	os << "┃\033[0m\n";
	for (int i=0;i<max;i++){
		os << "\033[0;47;30m┃" << i;
		for (int j=0;j<max;j++){
			if(go->Tab[i][j]==NULL){
				os << "─┼";
			}else{
				if(go->Tab[i][j]->color == true){
					os << "─\033[1;37;47m●\033[0;47;30m";
				}else{
					os << "─\033[0;30;47m●\033[0;47;30m";
				}
			}
		}
		os << "─┃\033[0m\n";
	}
	os << "\033[0;47;30m┗━━";
	for (int j=0;j<max;j++){
		os << "━━";
	}
		os << "┛\033[0m\n";
	os << "\n";
	return os;
}



int main( int argc, char** argv){
	cout << "\033[2J\033[0m"; //Formule magique qui vide le terminal.
	Begin();
	cout << "\n Merci d'avoir jouer ! \n \n";	
	return 0;
}
